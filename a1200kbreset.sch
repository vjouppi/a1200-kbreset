EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM339 U1
U 1 1 603CBB46
P 4800 4350
F 0 "U1" H 4800 3983 50  0000 C CNN
F 1 "LM339" H 4800 4074 50  0000 C CNN
F 2 "Package_SO:SO-14_3.9x8.65mm_P1.27mm" H 4750 4450 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm139.pdf" H 4850 4550 50  0001 C CNN
	1    4800 4350
	1    0    0    1   
$EndComp
$Comp
L Comparator:LM339 U1
U 2 1 603D2548
P 3500 4250
F 0 "U1" H 3500 3883 50  0000 C CNN
F 1 "LM339" H 3500 3974 50  0000 C CNN
F 2 "Package_SO:SO-14_3.9x8.65mm_P1.27mm" H 3450 4350 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm139.pdf" H 3550 4450 50  0001 C CNN
	2    3500 4250
	1    0    0    1   
$EndComp
$Comp
L Comparator:LM339 U1
U 3 1 603D3D2F
P 6650 4450
F 0 "U1" H 6650 4083 50  0000 C CNN
F 1 "LM339" H 6650 4174 50  0000 C CNN
F 2 "Package_SO:SO-14_3.9x8.65mm_P1.27mm" H 6600 4550 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm139.pdf" H 6700 4650 50  0001 C CNN
	3    6650 4450
	1    0    0    1   
$EndComp
$Comp
L Comparator:LM339 U1
U 4 1 603D6138
P 7950 4550
F 0 "U1" H 7950 4183 50  0000 C CNN
F 1 "LM339" H 7950 4274 50  0000 C CNN
F 2 "Package_SO:SO-14_3.9x8.65mm_P1.27mm" H 7900 4650 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm139.pdf" H 8000 4750 50  0001 C CNN
	4    7950 4550
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 603D7F5F
P 5100 6100
F 0 "#PWR0101" H 5100 5850 50  0001 C CNN
F 1 "GND" H 5105 5927 50  0000 C CNN
F 2 "" H 5100 6100 50  0001 C CNN
F 3 "" H 5100 6100 50  0001 C CNN
	1    5100 6100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 603D96D4
P 5100 5500
F 0 "#PWR0102" H 5100 5350 50  0001 C CNN
F 1 "VCC" H 5115 5673 50  0000 C CNN
F 2 "" H 5100 5500 50  0001 C CNN
F 3 "" H 5100 5500 50  0001 C CNN
	1    5100 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 603DD669
P 3050 4350
F 0 "R1" V 2843 4350 50  0000 C CNN
F 1 "1k" V 2934 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 2980 4350 50  0001 C CNN
F 3 "~" H 3050 4350 50  0001 C CNN
	1    3050 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 603DE66F
P 3150 5100
F 0 "R2" H 3220 5146 50  0000 L CNN
F 1 "1k" H 3220 5055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 3080 5100 50  0001 C CNN
F 3 "~" H 3150 5100 50  0001 C CNN
	1    3150 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 603DF566
P 3150 5250
F 0 "#PWR0103" H 3150 5000 50  0001 C CNN
F 1 "GND" H 3155 5077 50  0000 C CNN
F 2 "" H 3150 5250 50  0001 C CNN
F 3 "" H 3150 5250 50  0001 C CNN
	1    3150 5250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0104
U 1 1 603DFCB2
P 2900 4350
F 0 "#PWR0104" H 2900 4200 50  0001 C CNN
F 1 "VCC" H 2915 4523 50  0000 C CNN
F 2 "" H 2900 4350 50  0001 C CNN
F 3 "" H 2900 4350 50  0001 C CNN
	1    2900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4950 3150 4350
Wire Wire Line
	3150 4350 3200 4350
Connection ~ 3200 4350
$Comp
L Device:R R3
U 1 1 603E0887
P 4150 4100
F 0 "R3" H 4220 4146 50  0000 L CNN
F 1 "10k" H 4220 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 4080 4100 50  0001 C CNN
F 3 "~" H 4150 4100 50  0001 C CNN
	1    4150 4100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 603E1E34
P 4150 3950
F 0 "#PWR0105" H 4150 3800 50  0001 C CNN
F 1 "VCC" H 4165 4123 50  0000 C CNN
F 2 "" H 4150 3950 50  0001 C CNN
F 3 "" H 4150 3950 50  0001 C CNN
	1    4150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4250 4150 4250
Wire Wire Line
	4500 4250 4150 4250
Connection ~ 4150 4250
$Comp
L Device:CP C1
U 1 1 603E4008
P 4150 4400
F 0 "C1" H 4268 4446 50  0000 L CNN
F 1 "22uF" H 4268 4355 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 4188 4250 50  0001 C CNN
F 3 "~" H 4150 4400 50  0001 C CNN
	1    4150 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 603E4B7F
P 4150 4550
F 0 "#PWR0106" H 4150 4300 50  0001 C CNN
F 1 "GND" H 4155 4377 50  0000 C CNN
F 2 "" H 4150 4550 50  0001 C CNN
F 3 "" H 4150 4550 50  0001 C CNN
	1    4150 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 603E6E55
P 3500 5100
F 0 "C2" H 3615 5146 50  0000 L CNN
F 1 "0.1uF" H 3615 5055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3538 4950 50  0001 C CNN
F 3 "~" H 3500 5100 50  0001 C CNN
	1    3500 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4950 3500 4950
Connection ~ 3150 4950
Wire Wire Line
	3500 4950 4500 4950
Wire Wire Line
	4500 4950 4500 4450
Connection ~ 3500 4950
$Comp
L power:GND #PWR0107
U 1 1 603E86FC
P 3500 5250
F 0 "#PWR0107" H 3500 5000 50  0001 C CNN
F 1 "GND" H 3505 5077 50  0000 C CNN
F 2 "" H 3500 5250 50  0001 C CNN
F 3 "" H 3500 5250 50  0001 C CNN
	1    3500 5250
	1    0    0    -1  
$EndComp
Text GLabel 2300 4150 0    50   Input ~ 0
KB_CLK
Wire Wire Line
	2300 4150 3200 4150
Text GLabel 8700 4550 2    50   Input ~ 0
KB_RESET
Wire Wire Line
	8250 4550 8700 4550
$Comp
L Device:R R4
U 1 1 603EB669
P 5400 4200
F 0 "R4" H 5470 4246 50  0000 L CNN
F 1 "47k" H 5470 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5330 4200 50  0001 C CNN
F 3 "~" H 5400 4200 50  0001 C CNN
	1    5400 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:D D1
U 1 1 603EC023
P 5800 4200
F 0 "D1" V 5754 4280 50  0000 L CNN
F 1 "1N4148" V 5845 4280 50  0000 L CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5800 4200 50  0001 C CNN
F 3 "~" H 5800 4200 50  0001 C CNN
	1    5800 4200
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 603EDE9F
P 5400 4050
F 0 "#PWR0108" H 5400 3900 50  0001 C CNN
F 1 "VCC" H 5415 4223 50  0000 C CNN
F 2 "" H 5400 4050 50  0001 C CNN
F 3 "" H 5400 4050 50  0001 C CNN
	1    5400 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4350 5400 4350
Wire Wire Line
	5400 4350 5800 4350
Connection ~ 5400 4350
Wire Wire Line
	5800 4350 6350 4350
Connection ~ 5800 4350
$Comp
L Device:CP C3
U 1 1 603EFB52
P 5400 4500
F 0 "C3" H 5518 4546 50  0000 L CNN
F 1 "22uF" H 5518 4455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5438 4350 50  0001 C CNN
F 3 "~" H 5400 4500 50  0001 C CNN
	1    5400 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 603F0B74
P 5400 4650
F 0 "#PWR0109" H 5400 4400 50  0001 C CNN
F 1 "GND" H 5405 4477 50  0000 C CNN
F 2 "" H 5400 4650 50  0001 C CNN
F 3 "" H 5400 4650 50  0001 C CNN
	1    5400 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 603F12F5
P 6050 4800
F 0 "R5" V 5843 4800 50  0000 C CNN
F 1 "1k" V 5934 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5980 4800 50  0001 C CNN
F 3 "~" H 6050 4800 50  0001 C CNN
	1    6050 4800
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 603F1FCB
P 6700 4800
F 0 "R6" V 6493 4800 50  0000 C CNN
F 1 "1k" V 6584 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 6630 4800 50  0001 C CNN
F 3 "~" H 6700 4800 50  0001 C CNN
	1    6700 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 4950 5900 4950
Wire Wire Line
	5900 4950 5900 4800
Connection ~ 4500 4950
Wire Wire Line
	6200 4800 6350 4800
Wire Wire Line
	6350 4800 6350 4550
Wire Wire Line
	6350 4800 6550 4800
Connection ~ 6350 4800
$Comp
L Device:R R7
U 1 1 603F4760
P 7300 4300
F 0 "R7" H 7370 4346 50  0000 L CNN
F 1 "1k" H 7370 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 7230 4300 50  0001 C CNN
F 3 "~" H 7300 4300 50  0001 C CNN
	1    7300 4300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 603F53FD
P 7300 4150
F 0 "#PWR0110" H 7300 4000 50  0001 C CNN
F 1 "VCC" H 7315 4323 50  0000 C CNN
F 2 "" H 7300 4150 50  0001 C CNN
F 3 "" H 7300 4150 50  0001 C CNN
	1    7300 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4450 7300 4450
Wire Wire Line
	7300 4450 7650 4450
Connection ~ 7300 4450
Wire Wire Line
	6850 4800 7300 4800
Wire Wire Line
	7300 4800 7300 4450
Wire Wire Line
	5900 4950 7650 4950
Wire Wire Line
	7650 4950 7650 4650
Connection ~ 5900 4950
$Comp
L Comparator:LM339 U1
U 5 1 603D72D1
P 5200 5800
F 0 "U1" H 5158 5846 50  0000 L CNN
F 1 "LM339" H 5158 5755 50  0000 L CNN
F 2 "Package_SO:SO-14_3.9x8.65mm_P1.27mm" H 5150 5900 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/lm139.pdf" H 5250 6000 50  0001 C CNN
	5    5200 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4050 5400 4050
Connection ~ 5400 4050
Text GLabel 3300 3250 0    50   Input ~ 0
KB_CLK
Text GLabel 3300 3350 0    50   Input ~ 0
KB_RESET
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 6041AC41
P 3500 3350
F 0 "J1" H 3472 3232 50  0000 R CNN
F 1 "Conn_01x04_Male" H 3472 3323 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3500 3350 50  0001 C CNN
F 3 "~" H 3500 3350 50  0001 C CNN
	1    3500 3350
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 60420CA8
P 3300 3150
F 0 "#PWR0111" H 3300 3000 50  0001 C CNN
F 1 "VCC" H 3315 3323 50  0000 C CNN
F 2 "" H 3300 3150 50  0001 C CNN
F 3 "" H 3300 3150 50  0001 C CNN
	1    3300 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 60421969
P 3300 3450
F 0 "#PWR0112" H 3300 3200 50  0001 C CNN
F 1 "GND" H 3305 3277 50  0000 C CNN
F 2 "" H 3300 3450 50  0001 C CNN
F 3 "" H 3300 3450 50  0001 C CNN
	1    3300 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 603F7E0B
P 5500 5800
F 0 "C4" H 5615 5846 50  0000 L CNN
F 1 "0.1uF" H 5615 5755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 5538 5650 50  0001 C CNN
F 3 "~" H 5500 5800 50  0001 C CNN
	1    5500 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 604422EF
P 5500 5950
F 0 "#PWR?" H 5500 5700 50  0001 C CNN
F 1 "GND" H 5505 5777 50  0000 C CNN
F 2 "" H 5500 5950 50  0001 C CNN
F 3 "" H 5500 5950 50  0001 C CNN
	1    5500 5950
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 60442A45
P 5500 5650
F 0 "#PWR?" H 5500 5500 50  0001 C CNN
F 1 "VCC" H 5515 5823 50  0000 C CNN
F 2 "" H 5500 5650 50  0001 C CNN
F 3 "" H 5500 5650 50  0001 C CNN
	1    5500 5650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
