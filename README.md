# A1200 Keyboard Reset

A KiCAD project for getting keyboard reset to work with wedge Amigas when
using a big box keyboard.

The circuit has been redrawn from the A2000 schematics.

I have verified it to work on vero board, but haven't tried the PCB yet.

## Howto

R is reset out, C is KBClk in.
